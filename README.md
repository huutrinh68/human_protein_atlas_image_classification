# kaggle_human_protein_baseline

Human Protein Atlas Image Classification baseline:0.456

# 1. requirements

`pytorch-0.4.0` `pretrainedmodels-0.7.4` `imgaug`

# 2. usage

`step1: change config.py:train_data and test_data path for your self `

`step2: python main.py`

